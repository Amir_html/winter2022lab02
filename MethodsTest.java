public class MethodsTest{
	public static void main(String[] args){
		int x=10;
		System.out.println("x equals: "+x);
		methodNoInputNoReturn();
		System.out.println("x equals: "+x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(1,2.5);
		int z= methodNoInputReturnInt();
		System.out.println("The value of methodNoInputReturnInt() is: "+z);
		double y= sumSquareRoot(6,3);
		System.out.println("The value of sumSquareRoot(6,3) is: "+y);
		String s2="goodbye";
		String s1="hello";
		System.out.println("length of s1: "+s1.length());
		System.out.println("length of s2: "+s2.length());
		System.out.println("addOne(): "+SecondClass.addOne(50));
		SecondClass sc=new SecondClass();
		System.out.println("addTwo(): "+sc.addTwo(50));
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm  in a method that takes no input and returns nothing.");
		int x=50;
		System.out.println("x equals: "+x);
		
	}
	public static void methodOneInputNoReturn(int x){
		System.out.println("Inside the method one input no return");
		System.out.println("x is: "+x);
	}
	public static void methodTwoInputNoReturn(int x,double y){
		System.out.println("Inside the method two inputs no return");
		System.out.println("x is: "+x+" y is: "+y);
	}
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int x, int y){
		double z=x+y;
		z=Math.sqrt(z);
		return z;
	}
}